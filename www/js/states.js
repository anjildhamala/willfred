var states = angular.module("states", []);

states.config(function($stateProvider, $urlRouterProvider){

  $urlRouterProvider.otherwise("/landingPage");

  $stateProvider
    .state("landingPage", {
      url: "/landingPage",
      templateUrl: "../html/landing-page.html",
      controller: "landingCtrl"
  })
    .state("units",{
      url: "/units",
      templateUrl: "../html/units.html",
      controller: "unitsCtrl",
      params: {
        units: 1
      }
    })

});
