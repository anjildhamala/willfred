var controllers = angular.module("controllers", []);

  controllers.controller("landingCtrl", function($scope, $cordovaDialogs,$state) {

    $scope.getStarted = function () {

      $cordovaDialogs.prompt('Enter number of units to evaluate', 'title', ['btn 1','btn 2'], '1-20')
        .then(function(result) {
          var input = result.input1;

          if(result.buttonIndex === 2){
            return;
          } else if(input > 0 && input <= 20) {
            $state.go("units", {units: input});
          } else {
            $cordovaDialogs.alert("Please try again", "invalid entry", "ok").then($scope.getStarted);
          }
        });
    };

  });

  controllers.controller("unitsCtrl", function($scope, $stateParams){

    $scope.createUnits = function(){
      var dummyArray = [];
      for(var i = 0; i < $stateParams.units; i++){
        dummyArray[i] = i;
      }
      return dummyArray;
    };


  });
